package com.siemens.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.siemens.powerplant.dto.AggregateDTO;
import com.siemens.powerplant.entity.Aggregate;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.repository.AggregateRepository;
import com.siemens.powerplant.service.AggregateService;

@Component
public class AggregateServiceImpl implements AggregateService {

	@Autowired
	private AggregateRepository aggregateRepository;

	@Override
	public void createAggregates(AggregateDTO aggregateDTO) {
		
		Aggregate aggregate = new Aggregate();
		if (null != aggregateDTO) {
			
			if (null != aggregateDTO.getAggregate_name() && null != aggregateDTO.getAggregate_type()) {
				aggregate.setAggregate_name(aggregateDTO.getAggregate_name());
				aggregate.setAggregate_type(aggregateDTO.getAggregate_type());

			} else {
				throw new RuntimeException("Parameters are not present");
			}
			aggregateRepository.save(aggregate);
		}
		
	}

	@Override
	public Aggregate getAggregateById(int id) throws PowerPlantException {
		Aggregate ag = null;
		Object optional = aggregateRepository.findById(id);
		try
		{
			ag = ((Optional<Aggregate>) optional).get();
		}catch(NoSuchElementException e)
		{
			throw new PowerPlantException("Aggregate id not valid");
		}

		return ag;
	}

	@Override
	public List<Aggregate> findAllAggregates() {
		List<Aggregate> allAggregates = new ArrayList<Aggregate>();
		allAggregates =  (List<Aggregate>) aggregateRepository.findAll();
		return allAggregates;
	}

	@Override
	public ResponseEntity<Aggregate> updateAggregate(int id, AggregateDTO aggregateDTO) {
		Optional<Aggregate> aggregateOptional = aggregateRepository.findById(id);
		Aggregate aggregate = null;
		if(aggregateOptional.isPresent()){
			aggregate = aggregateOptional.get();

			if(null != aggregateDTO.getAggregate_name()){
				aggregate.setAggregate_name(aggregateDTO.getAggregate_name());
			}
			if(null != aggregateDTO.getAggregate_type()){
				aggregate.setAggregate_type(aggregateDTO.getAggregate_type());
			}
			aggregateRepository.save(aggregate);
		}	
		return new ResponseEntity<Aggregate>(aggregate,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> deleteAggregate(int id) throws PowerPlantException {

		Iterable<Aggregate> mc = aggregateRepository.findAll();
		Iterator<Aggregate> iterator = mc.iterator();
		String msg = "Deleted Successfully";
		List<Integer> aggregateIds = new ArrayList<Integer>();
		boolean flag = false;
		while(iterator.hasNext())
		{
			Object aggregateObject = iterator.next();
			aggregateIds.add(((Aggregate) aggregateObject).getAggregate_id());
		}
		if(aggregateIds.contains(id))
		{
			aggregateRepository.deleteById(id);
			flag = true;
		}
		else
		{
			throw new PowerPlantException("Id for which u want to delete Aggregate is invalid.");
		}
		if(flag == true)
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		else
		{return null;}
	}

}


