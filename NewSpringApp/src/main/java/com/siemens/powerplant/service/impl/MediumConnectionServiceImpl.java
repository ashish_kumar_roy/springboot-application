package com.siemens.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.siemens.powerplant.dto.MediumConnectionDTO;
import com.siemens.powerplant.entity.Aggregate;
import com.siemens.powerplant.entity.MediumConnection;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.repository.AggregateRepository;
import com.siemens.powerplant.repository.MediumConnectionRepository;
import com.siemens.powerplant.service.MediumConnectionService;

@Service
public class MediumConnectionServiceImpl implements MediumConnectionService{

	@Autowired
	private MediumConnectionRepository mediumConnectionRepository;

	@Autowired
	private AggregateRepository aggregateRepository;


	@Override
	public List<MediumConnection> getAllMediumConnection() {
		return (List<MediumConnection>) mediumConnectionRepository.findAll();

	}

	@Override
	public ResponseEntity<MediumConnection> getMediumConnectionById(int id) throws PowerPlantException {
		MediumConnection mediumConnection = null;
		Optional<MediumConnection> optional = mediumConnectionRepository.findById(id);
		try
		{
			mediumConnection = optional.get();
		}catch(NoSuchElementException e)
		{
			throw new PowerPlantException("Medium connection id is not valid");
		}

		return new ResponseEntity<MediumConnection>(mediumConnection,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> createMediumConnection(MediumConnectionDTO mediumConnectionDTO) throws PowerPlantException {

		int saId = mediumConnectionDTO.getSAId();
		int daId = mediumConnectionDTO.getDAId();
		MediumConnection mediumConnection = new MediumConnection();
		Iterable<Aggregate> agDetails = aggregateRepository.findAll();
		Iterator<Aggregate> iterator = agDetails.iterator();
		List<Integer> aggergateIds = new ArrayList<Integer>();
		while(iterator.hasNext())
		{
			Object aggregate = iterator.next();
			aggergateIds.add(((Aggregate)aggregate).getAggregate_id());
		}

		if(aggergateIds.contains(mediumConnectionDTO.getDAId()) && (aggergateIds.contains(mediumConnectionDTO.getSAId())))
		{
			mediumConnection.setDAId(mediumConnectionDTO.getDAId());
			mediumConnection.setSAId(mediumConnectionDTO.getSAId());

		}
		else
		{
			throw new PowerPlantException("Aggregate id Entered is not correct..");

		}
		if(mediumConnectionDTO.getMCType() != null && mediumConnectionDTO.getSPort() != null && mediumConnectionDTO.getDPort() != null)
		{
			mediumConnection.setMCType(mediumConnectionDTO.getMCType());
			mediumConnection.setSPort(mediumConnectionDTO.getSPort());
			mediumConnection.setDPort(mediumConnectionDTO.getDPort());
			mediumConnectionRepository.save(mediumConnection);
		}
		else
		{
			throw new PowerPlantException("Either type or Source port or Destination Port is null");
		}

		return new ResponseEntity<String>("Created Successfully", HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<String> deleteMediumConnection(int id) throws PowerPlantException {

		Iterable<MediumConnection> mc = mediumConnectionRepository.findAll();
		Iterator<MediumConnection> iterator = mc.iterator();
		String msg = "Deleted Successfully";
		List<Integer> mediumConnectionIds = new ArrayList<Integer>();
		boolean flag = false;
		while(iterator.hasNext())
		{
			Object mediumConnectionObject = iterator.next();
			mediumConnectionIds.add(((MediumConnection) mediumConnectionObject).getMCId());
		}
		if(mediumConnectionIds.contains(id))
		{
			mediumConnectionRepository.deleteById(id);
			flag = true;
		}
		else
		{
			throw new PowerPlantException("Id for which u want to delete medium connection is invalid.");
		}
		if(flag == true)
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		else
		{return null;}
	}

	@Override
	public ResponseEntity<String> updateMediumConnection(int id, MediumConnectionDTO mediumConnectionDTO) throws PowerPlantException {

		MediumConnection mediumConnection = null;
		Optional<MediumConnection> optional = mediumConnectionRepository.findById(id);
		if(optional.isPresent()){
			
			mediumConnection = optional.get();
			Iterable<Aggregate> agDetails = aggregateRepository.findAll();
			Iterator<Aggregate> iterator1 = agDetails.iterator();
			List<Integer> aggergateIds = new ArrayList<Integer>();
			while(iterator1.hasNext())
			{
				Object aggregate = iterator1.next();
				aggergateIds.add(((Aggregate)aggregate).getAggregate_id());
			}

			if(aggergateIds.contains(mediumConnectionDTO.getDAId()) && (aggergateIds.contains(mediumConnectionDTO.getSAId())))
			{
				mediumConnection.setDAId(mediumConnectionDTO.getDAId());
				mediumConnection.setSAId(mediumConnectionDTO.getSAId());
			}
			else
			{
				throw new PowerPlantException("Aggregate id Entered is not correct..");
			}
			if(mediumConnectionDTO.getMCType() != null && mediumConnectionDTO.getDPort() != null && mediumConnectionDTO.getSPort() != null)
			{
				mediumConnection.setMCType(mediumConnectionDTO.getMCType());
				mediumConnection.setSPort(mediumConnectionDTO.getSPort());
				mediumConnection.setDPort(mediumConnectionDTO.getDPort());
			}
			else
			{
				throw new PowerPlantException("Either type or source port or destination port is null");
			}
			mediumConnectionRepository.save(mediumConnection);
		}

		return new ResponseEntity<String>("Updated Successfully.", HttpStatus.OK);

	}



}
