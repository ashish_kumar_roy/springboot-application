package com.siemens.powerplant.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.siemens.powerplant.dto.CycleSegmentDTO;
import com.siemens.powerplant.entity.CycleSegment;
import com.siemens.powerplant.exceptions.PowerPlantException;

public interface CycleSegmentService {
	
	public ResponseEntity<String> createCycleSegment(CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException;
	public List<CycleSegment> findAllCycleSegment();
	public CycleSegment getCycleSegmentById(int id) throws PowerPlantException;
	public ResponseEntity<String> deleteCycleSegment(int id) throws PowerPlantException;
	public CycleSegment updateCycleSegment(int id, CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException;
}
