package com.siemens.powerplant.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.siemens.powerplant.dto.CycleSegmentDTO;
import com.siemens.powerplant.entity.Aggregate;
import com.siemens.powerplant.entity.CycleSegment;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.repository.AggregateRepository;
import com.siemens.powerplant.repository.CycleSegmentRepository;
import com.siemens.powerplant.service.CycleSegmentService;

@Service
public class CycleSegmentServiceImpl implements CycleSegmentService{

	@Autowired
	private AggregateRepository aggregateRepository;

	@Autowired
	private CycleSegmentRepository cycleSegmentRepository;

	@Override
	public ResponseEntity<String> createCycleSegment(CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException {

		String msg = "Created Successfully";
		boolean flag = false;
		Iterable<Aggregate> agDetails = aggregateRepository.findAll();
		Iterator<Aggregate> iterator = agDetails.iterator();
		List<Integer> aggergateIds = new ArrayList<Integer>();
		CycleSegment cycleSegment = new CycleSegment();

		while(iterator.hasNext())
		{
			Object aggregate = iterator.next();
			aggergateIds.add(((Aggregate)aggregate).getAggregate_id());
		}

		if(aggergateIds.contains(cycleSegmentDTO.getAGID()) && cycleSegmentDTO.getCSName() != null && cycleSegmentDTO.getCSType()!= null)
		{
			cycleSegment.setAGID(cycleSegmentDTO.getAGID());
			cycleSegment.setCSName(cycleSegmentDTO.getCSName());
			cycleSegment.setCSType(cycleSegmentDTO.getCSType());

			cycleSegmentRepository .save(cycleSegment);
			flag = true;
		}
		else
		{
			throw new PowerPlantException("Aggregate id Entered is not correct..");

		}
		if(flag == true)
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		else
		{return null;}
	}

	@Override
	public List<CycleSegment> findAllCycleSegment() {
		return (List<CycleSegment>) cycleSegmentRepository.findAll();
	}

	@Override
	public CycleSegment getCycleSegmentById(int id) throws PowerPlantException {
		CycleSegment cycleSegment = null;
		Optional<CycleSegment> optional = cycleSegmentRepository.findById(id);
		try
		{
			cycleSegment = optional.get();
		}catch(NoSuchElementException e)
		{
			throw new PowerPlantException("CycleSegment id is not valid");
		}

		return cycleSegment;
	}

	@Override
	public ResponseEntity<String> deleteCycleSegment(int id) throws PowerPlantException {
		Iterable<CycleSegment> mc = cycleSegmentRepository.findAll();
		Iterator<CycleSegment> iterator = mc.iterator();
		String msg = "Deleted Successfully";
		List<Integer> cycleSegmentIds = new ArrayList<Integer>();
		boolean flag = false;
		while(iterator.hasNext())
		{
			Object cycleSegmentObject = iterator.next();
			cycleSegmentIds.add(((CycleSegment) cycleSegmentObject).getCSId());
		}
		if(cycleSegmentIds.contains(id))
		{
			cycleSegmentRepository.deleteById(id);
			flag = true;
		}
		else
		{
			throw new PowerPlantException("Id for which u want to delete medium connection is invalid.");
		}
		if(flag == true)
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		else
		{return null;}
	}

	@Override
	public CycleSegment updateCycleSegment(int id, CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException {
		Optional<CycleSegment> cyclesegment =  cycleSegmentRepository.findById(id);
		CycleSegment cs = null;
		List<Integer> aggregateIds = new ArrayList<Integer>();
		if(cyclesegment.isPresent())
		{
			cs = cyclesegment.get();
			if(0 != cycleSegmentDTO.getAGID()){
				Iterable<Aggregate> array = aggregateRepository.findAll();
				Iterator iterator =  array.iterator();
				while(iterator.hasNext()){
					Object aggregate = iterator.next();
					aggregateIds.add(((Aggregate) aggregate).getAggregate_id());
				}
			}

			if(aggregateIds.contains(cycleSegmentDTO.getAGID()))
			{
				cs.setAGID(cycleSegmentDTO.getAGID());
			}
			else
			{
				throw new PowerPlantException("Aggregate Id entered is incorrect");
			}
			if(cycleSegmentDTO.getCSName() != null && cycleSegmentDTO.getCSType() != null){
				cs.setCSName(cycleSegmentDTO.getCSName());
				cs.setCSType(cycleSegmentDTO.getCSType());
			}
			else
			{
				throw new PowerPlantException("Name or type is null");
			}

			cycleSegmentRepository.save(cs);
		}
		return cs;

	}


}
