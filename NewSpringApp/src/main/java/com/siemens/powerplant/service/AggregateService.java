package com.siemens.powerplant.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.siemens.powerplant.dto.AggregateDTO;
import com.siemens.powerplant.entity.Aggregate;
import com.siemens.powerplant.exceptions.PowerPlantException;


public interface AggregateService {

	public void createAggregates(AggregateDTO aggregateDTO);
	public Aggregate getAggregateById(int id) throws PowerPlantException;
	public List<Aggregate> findAllAggregates();
	public ResponseEntity<Aggregate> updateAggregate(int id, AggregateDTO aggregateDTO);
	public ResponseEntity<String> deleteAggregate(int id) throws PowerPlantException;
}
