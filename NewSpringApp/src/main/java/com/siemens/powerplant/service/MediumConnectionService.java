package com.siemens.powerplant.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.siemens.powerplant.dto.MediumConnectionDTO;
import com.siemens.powerplant.entity.MediumConnection;
import com.siemens.powerplant.exceptions.PowerPlantException;

public interface MediumConnectionService {

	public List<MediumConnection> getAllMediumConnection();
	public ResponseEntity<MediumConnection> getMediumConnectionById(int id) throws PowerPlantException;
	public ResponseEntity<String> createMediumConnection(MediumConnectionDTO connectionDTO) throws PowerPlantException;
	public ResponseEntity<String> deleteMediumConnection(int id) throws PowerPlantException;
	public ResponseEntity<String> updateMediumConnection(int id, MediumConnectionDTO connectionDTO) throws PowerPlantException;
	
}
