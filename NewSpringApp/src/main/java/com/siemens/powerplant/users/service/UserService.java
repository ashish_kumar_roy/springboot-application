package com.siemens.powerplant.users.service;

import com.siemens.powerplant.model.User;

public interface UserService {
	public User save(User user);

	public User findByEmail(String email);

}
