package com.siemens.powerplant.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.siemens.powerplant.dto.AggregateDTO;
import com.siemens.powerplant.entity.Aggregate;


@Component
public interface AggregateRepository extends CrudRepository<Aggregate, Integer>{

	void save(AggregateDTO aggregateDTO);


}
