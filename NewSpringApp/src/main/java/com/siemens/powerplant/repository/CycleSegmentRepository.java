package com.siemens.powerplant.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.siemens.powerplant.dto.CycleSegmentDTO;
import com.siemens.powerplant.entity.CycleSegment;

@Repository
public interface CycleSegmentRepository extends CrudRepository<CycleSegment, Integer>{

	void save(CycleSegmentDTO cycleSegmentDTO);

}
