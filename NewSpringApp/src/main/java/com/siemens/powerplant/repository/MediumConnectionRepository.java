package com.siemens.powerplant.repository;

import org.springframework.data.repository.CrudRepository;

import com.siemens.powerplant.entity.MediumConnection;

public interface MediumConnectionRepository extends CrudRepository<MediumConnection , Integer>{

}
