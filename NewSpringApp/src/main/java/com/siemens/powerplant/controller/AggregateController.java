package com.siemens.powerplant.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siemens.powerplant.dto.AggregateDTO;
import com.siemens.powerplant.entity.Aggregate;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.repository.AggregateRepository;
import com.siemens.powerplant.service.AggregateService;

@SuppressWarnings("unused")
@RestController
public class AggregateController {

	@Autowired
	private AggregateService aggregateService;

	@PostMapping("/createAggregates")
	public void createAggregates(@RequestBody AggregateDTO aggregateDTO)
	{
		aggregateService.createAggregates(aggregateDTO);
	}

	@GetMapping("/findAllAggregates")
	public List<Aggregate> findAllAggregates()
	{
		List<Aggregate> allAggregates =  aggregateService.findAllAggregates();

		return allAggregates;
	}

	//get a single aggregate
	@GetMapping("/aggregates/{id}")
	public Aggregate getAggregateById(@PathVariable(value = "id") Integer id ) throws PowerPlantException
	{
		Aggregate ag = aggregateService.getAggregateById(id);
		return ag;
	}

	//update aggregate
	@RequestMapping(value = "/aggregates/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Aggregate> updateAggregate(@PathVariable(value = "id") Integer id,@RequestBody AggregateDTO aggregateDTO) 
	{
		ResponseEntity<Aggregate> entity = aggregateService.updateAggregate(id, aggregateDTO);
		return entity;

	}

	@DeleteMapping("/aggregates/{id}")
	public ResponseEntity<String> deleteAggregate(@PathVariable(value = "id")Integer id) throws PowerPlantException 
	{
		ResponseEntity<String> entity = aggregateService.deleteAggregate(id);
		return entity;
		
	}
}
