package com.siemens.powerplant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siemens.powerplant.dto.MediumConnectionDTO;
import com.siemens.powerplant.entity.MediumConnection;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.service.MediumConnectionService;

@RestController
public class MediumConnectionController {

	
	@Autowired
	private MediumConnectionService mediumConnectionService;
	
	@RequestMapping(value = "/getAllMediumConnection", method = RequestMethod.GET)
	public List<MediumConnection> getAllMediumConnection()
	{
		List<MediumConnection> mediumConnections = mediumConnectionService.getAllMediumConnection();
		return mediumConnections;
	}

	@RequestMapping(value = "/getMediumConnectionById/{id}", method = RequestMethod.GET)
	public ResponseEntity<MediumConnection> getMediumConnectionById(@PathVariable (value = "id") Integer id) throws PowerPlantException
	{	
		ResponseEntity<MediumConnection> entity = mediumConnectionService.getMediumConnectionById(id);
		return entity;
	}

	@RequestMapping(value = "/createMediumConnection", method = RequestMethod.POST)
	public ResponseEntity<String> createMediumConnection(@RequestBody MediumConnectionDTO mediumConnectionDTO) throws PowerPlantException
	{
		ResponseEntity<String> entity = mediumConnectionService.createMediumConnection(mediumConnectionDTO);
		return entity;
	}

	@RequestMapping(value = "/deleteMediumConnection/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteMediumConnection(@PathVariable (value = "id") Integer id) throws PowerPlantException
	{
		ResponseEntity<String> entity = mediumConnectionService.deleteMediumConnection(id);
		return entity;
	}

	@RequestMapping(value = "/updateMediumConnection/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateMediumConnection(@PathVariable (value = "id") Integer id, @RequestBody MediumConnectionDTO mediumConnectionDTO) throws PowerPlantException
	{
		ResponseEntity<String> entity = mediumConnectionService.updateMediumConnection(id, mediumConnectionDTO);
		return entity;
	}
}
