package com.siemens.powerplant.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.exceptions.Response;

@ControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(PowerPlantException.class)
	public ResponseEntity<Response> handleMyException(Exception e){
		Response response = new Response();
		response.setMsg(e.getMessage());
		return new ResponseEntity<Response>(response, HttpStatus.NOT_FOUND);
	}

}
