package com.siemens.powerplant.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siemens.powerplant.dto.CycleSegmentDTO;
import com.siemens.powerplant.entity.CycleSegment;
import com.siemens.powerplant.exceptions.PowerPlantException;
import com.siemens.powerplant.service.CycleSegmentService;

@RestController
public class CycleSegmentController {

	@Autowired
	private CycleSegmentService cycleSegmentService;


	@PostMapping("/createCycleSegment")
	public ResponseEntity<String> createCycleSegment(@Valid @RequestBody CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException
	{
		ResponseEntity<String> entity = cycleSegmentService.createCycleSegment(cycleSegmentDTO);
		return entity;
	}

	@GetMapping("/findAllCycleSegment")
	public List<CycleSegment> findAllCycleSegment()
	{
		List<CycleSegment> cycleSegments = cycleSegmentService.findAllCycleSegment();
		return cycleSegments;
	}

	@GetMapping("/cyclesegment/{id}")
	public CycleSegment getCycleSegmentById(@PathVariable(value = "id") Integer id) throws PowerPlantException
	{
		CycleSegment cycleSegment = cycleSegmentService.getCycleSegmentById(id);
		return cycleSegment;
	}

	@DeleteMapping("/cyclesegment/{id}")
	public ResponseEntity<String> deleteCycleSegment(@PathVariable (value = "id") Integer id) throws PowerPlantException
	{
		ResponseEntity<String> entity = cycleSegmentService.deleteCycleSegment(id);
		return entity;
	}

	@RequestMapping(value = "cyclesegment/{id}", method = RequestMethod.PUT)
	public CycleSegment updateCycleSegment(@PathVariable (value = "id") Integer id, @RequestBody CycleSegmentDTO cycleSegmentDTO) throws PowerPlantException
	{
		CycleSegment cycleSegment = cycleSegmentService.updateCycleSegment(id, cycleSegmentDTO);
		return cycleSegment;
	}
}
