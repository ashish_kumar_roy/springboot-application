package com.siemens.powerplant.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.siemens.powerplant.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
	public User save(User user);

	public User findByEmail(String email);
}
