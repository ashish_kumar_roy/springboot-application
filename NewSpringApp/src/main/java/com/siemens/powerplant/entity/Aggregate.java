package com.siemens.powerplant.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Aggregate	{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int aggregate_id;
	private String aggregate_name;
	private String aggregate_type;
	public int getAggregate_id() {
		return aggregate_id;
	}
	public void setAggregate_id(int aggregate_id) {
		this.aggregate_id = aggregate_id;
	}
	public String getAggregate_name() {
		return aggregate_name;
	}
	public void setAggregate_name(String aggregate_name) {
		this.aggregate_name = aggregate_name;
	}
	public String getAggregate_type() {
		return aggregate_type;
	}
	public void setAggregate_type(String aggregate_type) {
		this.aggregate_type = aggregate_type;
	}

	


}
