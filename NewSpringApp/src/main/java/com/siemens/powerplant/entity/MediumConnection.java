package com.siemens.powerplant.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mediumconnection")
public class MediumConnection {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int MCId;
	
	private String MCType;
	private int SAId;
	private int DAId;
	private String SPort;
	private String DPort;
	
	public int getMCId() {
		return MCId;
	}
	public void setMCId(int mCId) {
		MCId = mCId;
	}
	public String getMCType() {
		return MCType;
	}
	public void setMCType(String mCType) {
		MCType = mCType;
	}
	public int getSAId() {
		return SAId;
	}
	public void setSAId(int sAId) {
		SAId = sAId;
	}
	public int getDAId() {
		return DAId;
	}
	public void setDAId(int dAId) {
		DAId = dAId;
	}
	public String getSPort() {
		return SPort;
	}
	public void setSPort(String sPort) {
		SPort = sPort;
	}
	public String getDPort() {
		return DPort;
	}
	public void setDPort(String dPort) {
		DPort = dPort;
	}
}
