package com.siemens.powerplant.dto;

public class CycleSegmentDTO {
	
private int CSId;
	
	private String CSName;
	private String CSType;
	private int AGID;
	public int getCSId() {
		return CSId;
	}
	public void setCSId(int cSId) {
		CSId = cSId;
	}
	public String getCSName() {
		return CSName;
	}
	public void setCSName(String cSName) {
		CSName = cSName;
	}
	public String getCSType() {
		return CSType;
	}
	public void setCSType(String cSType) {
		CSType = cSType;
	}
	public int getAGID() {
		return AGID;
	}
	public void setAGID(int aGID) {
		AGID = aGID;
	}
}
