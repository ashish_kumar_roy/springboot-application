package com.siemens.powerplant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.siemens.powerplant.config.JwtFilter;

@SpringBootApplication
public class Application {
	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new com.siemens.powerplant.config.JwtFilter());
		registrationBean.addUrlPatterns("/secure/*");

		return registrationBean;
	}
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
	}
}
