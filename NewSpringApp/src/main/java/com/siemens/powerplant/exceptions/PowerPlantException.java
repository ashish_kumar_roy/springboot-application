package com.siemens.powerplant.exceptions;

public class PowerPlantException extends Exception{

	public PowerPlantException(String str)
	{
		super(str);
	}
}
